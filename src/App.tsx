import React, { useState } from "react";

import { Field, Form } from "react-final-form";
import styled, { css } from "styled-components";

import { Column, InputField } from "./components";
import { PopUp } from "./components";
import { selectors, actions, useAppSelector, useAppDispatch } from "./store";

const validateUser = (values: { userName: string }) => {
  const error = { userName: "Required field" };
  if (!values.userName) {
    return error;
  }
};

const App: React.FC = () => {
  const [modalActive, setModalActive] = useState(true);
  const user = useAppSelector(selectors.user.getUser);
  const columns = useAppSelector(selectors.column.getAllColumns);
  const dispatch = useAppDispatch();

  const handleModalUser = (values: { userName: string }) => {
    setModalActive(false);
    dispatch(actions.user.addUser({ userName: values.userName }));
  };

  const renderColumns = () => {
    return columns.map((column) => {
      return <Column id={column.id} name={column.name} key={column.id} />;
    });
  };

  const checkUserName = () => {
    if (!user) {
      return (
        <PopUp active={modalActive} setActive={() => null}>
          <User>
            Input your name
            <Form
              onSubmit={handleModalUser}
              initialValues={{ userName: "" }}
              validate={validateUser}
              render={({ handleSubmit, pristine }) => (
                <UserForm onSubmit={handleSubmit}>
                  <Field
                    name="userName"
                    component={InputField}
                    placeholder="username"
                    CSS={UserStyles}
                  />
                  <ButtonNew disabled={pristine}>ok</ButtonNew>
                </UserForm>
              )}
            />
          </User>
        </PopUp>
      );
    }
  };

  return (
    <Root>
      {checkUserName()}
      <DivColumns>{renderColumns()}</DivColumns>
    </Root>
  );
};

export default App;

const DivColumns = styled.div`
  display: flex;
  justify-content: space-between;
`;

const ButtonNew = styled.button`
  width: 100px;
  height: 30px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 10px;
  margin-left: 55px;
  margin-top: 20px;
`;

const UserForm = styled.form`
  margin-top: 10px;
`;
const UserStyles = css`
  background-color: white;
  font-weight: 300;
`;

const Root = styled.div`
  background: white;
`;
const User = styled.div`
  background: rgb(0, 0, 0, 0.2);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 80%;
  height: 20vw;
  border-radius: 10px;
`;
