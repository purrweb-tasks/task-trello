export type Card = {
  name: string;
  id: string;
  columnId: string;
  author: string;
  description: string;
  comments: Comment[];
};

export type Column = {
  id: string;
  name: string;
  cards: Card[];
};

export type Comment = {
  id: string;
  idCard: string;
  author: string;
  content: string;
};
