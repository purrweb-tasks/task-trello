import { RootState } from "../..";

export const getAllColumns = (state: RootState) => state.column.columns;
