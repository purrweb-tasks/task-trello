import React from "react";

import { FieldRenderProps } from "react-final-form";
import styled, { CSSProp } from "styled-components";

const InputField: React.FC<InputProps> = ({
  input,
  isTextarea = false,
  meta,
  ...rest
}) => {
  const hasError = meta.error && meta.touched;
  return (
    <>
      {isTextarea ? (
        <TextArea {...input} {...rest} />
      ) : (
        <Input {...input} {...rest} />
      )}
      {hasError && <ErrorMessage>{meta.error}</ErrorMessage>}
    </>
  );
};

export default InputField;

interface InputProps extends FieldRenderProps<string> {
  isTextarea?: boolean;
}
const Input = styled.input<{ CSS?: CSSProp }>`
  background: rgb(0, 0, 0, 0);
  padding: 5px;
  border-radius: 10px;
  border: none;
  text-align: center;
  font-family: "Franklin Gothic Medium";
  font-weight: 800;
  font-size: larger;
  border: 1px rgb(0, 0, 0, 0.2) solid;
  :hover {
    border: 1px rgb(0, 0, 0, 0.5) solid;
  }
  ${({ CSS }) => CSS}
`;
const ErrorMessage = styled.div`
  color: red;
  font-size: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;
`;
const TextArea = styled.textarea<{ CSS?: CSSProp }>`
  border-radius: 7px;
  border: 1px rgb(0, 0, 0, 0.4) solid;
  resize: none;
  width: 300px;
  height: 50px;
  ${({ CSS }) => CSS}
`;
