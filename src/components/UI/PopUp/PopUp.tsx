import React from "react";

import styled from "styled-components";
const PopUp: React.FC<PopUpProps> = ({ active, setActive, children }) => {
  if (!active) {
    return null;
  }

  return (
    <Root onClick={() => setActive(false)}>
      <PopUpContent onClick={(e) => e.stopPropagation()}>
        {children}
      </PopUpContent>
    </Root>
  );
};

export default PopUp;

type PopUpProps = {
  active: boolean;
  setActive: (active: boolean) => void;
  children: React.ReactNode;
};

const Root = styled.div`
  height: 100vh;
  width: 100vw;
  background-color: rgba(0, 0, 0, 0.4);
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  top: 0;
  left: 0;
`;
const PopUpContent = styled.div`
  padding: 20px;
  border-radius: 12px;
  background-color: white;
  max-height: max-content;
  min-height: 200px;
  min-width: 500px;
  overflow-y: scroll;
  max-width: max-content;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  pointer-events: all;
`;
