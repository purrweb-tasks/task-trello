import React from "react";

import { Field, Form } from "react-final-form";
import styled from "styled-components";

import { actions, useAppDispatch } from "../../store";
import { InputField } from "../UI";

const validateComment = (values: { contentComment: string }) => {
  const error = { contentComment: "Required field" };
  if (!values.contentComment) {
    return error;
  }
};

const Comment: React.FC<CommentProps> = ({ id, author, content }) => {
  const dispatch = useAppDispatch();

  const handleDeleteComment = () => {
    dispatch(actions.comment.deleteComment({ idComment: id }));
  };
  const handleUpdateComment = (values: { contentComment: string }) => {
    dispatch(
      actions.comment.updateComment({
        idComment: id,
        newContent: values.contentComment,
      })
    );
  };
  return (
    <Root>
      <Author>{author}</Author>
      <Form
        onSubmit={handleUpdateComment}
        initialValues={{ contentComment: content }}
        validate={validateComment}
        render={({ handleSubmit, form }) => (
          <Field
            name="contentComment"
            component={InputField}
            onBlur={() => {
              handleSubmit();
              form.reset();
            }}
            isTextarea
          />
        )}
      />
      <CommentButton onClick={handleDeleteComment}>delete</CommentButton>
    </Root>
  );
};

export default Comment;

type CommentProps = {
  idCard: string;
  id: string;
  author: string;
  content: string;
};

const Root = styled.div`
  display: flex;
  margin-bottom: 10px;
  margin-right: 15px;
  display: flex;
  flex-direction: column;
`;
const Author = styled.div`
  padding: 10px;
`;
const CommentButton = styled.button`
  background-color: rgb(255, 0, 0, 0.2);
  border-radius: 10px;
  margin-left: 10px;
  width: 50px;
  cursor: pointer;
  margin-top: 10px;
`;
