import React, { useState } from "react";

import { Field, Form } from "react-final-form";
import styled, { css } from "styled-components";

import {
  selectors,
  actions,
  useAppSelector,
  useAppDispatch,
} from "../../store";
import { Comment as CommentType } from "../../types";
import FullCard from "../FullCard";
import { InputField, PopUp } from "../UI";

const validateCardName = (values: { cardName: string }) => {
  const error = { cardName: "Required field" };
  if (!values.cardName) {
    return error;
  }
};

const Card: React.FC<CardProps> = ({ id, name, description }) => {
  const dispatch = useAppDispatch();
  const [activeFullCard, setActiveFullCard] = useState(false);
  const author = useAppSelector(selectors.user.getUser);
  const comments = useAppSelector(selectors.comment.selectByCardId(id));

  const handleDeleteCard = async () => {
    dispatch(actions.card.deleteCard({ idCard: id }));
  };

  const handleUpdateName = (values: { cardName: string }) => {
    dispatch(
      actions.card.updateNameCard({ idCard: id, newName: values.cardName })
    );
  };

  return (
    <Root
      onKeyDown={(event: React.KeyboardEvent<HTMLDivElement>) => {
        if (event.keyCode === 27) {
          setActiveFullCard(false);
        }
      }}
    >
      <PopUp active={activeFullCard} setActive={setActiveFullCard}>
        <FullCard
          idCard={id}
          name={name}
          author={author}
          description={description}
        />
      </PopUp>
      <Form
        onSubmit={handleUpdateName}
        initialValues={{ cardName: name }}
        validate={validateCardName}
        render={({ handleSubmit, form }) => (
          <Field
            name="cardName"
            component={InputField}
            onBlur={() => {
              handleSubmit();
              form.reset();
            }}
            CSS={CardNameStyles}
          />
        )}
      />
      <CommentsCard>{comments.length} comments</CommentsCard>
      <Buttons>
        <Button isDeleteType onClick={handleDeleteCard}>
          delete
        </Button>
        <Button onClick={() => setActiveFullCard(true)}>expand</Button>
      </Buttons>
    </Root>
  );
};

export default Card;

type CardProps = {
  id: string;
  name: string;
  author: string;
  description: string;
  comments: CommentType[];
};
interface ButtonProps {
  isDeleteType?: boolean;
}
const Root = styled.div`
  background: rgb(255, 255, 255, 0.9);
  border-radius: 7px;
  border: 1px rgb(255, 255, 255, 0.1) solid;
  margin-bottom: 30px;
`;
const CardNameStyles = css`
  background-color: rgb(0, 0, 0, 0);
  font-weight: 900;
  font-size: 16px;
  padding: 5px;
  border-radius: 10px;
  border: none;
  text-align: center;
  margin: 5px;
  :hover {
    border: none;
    background-color: rgb(0, 0, 0, 0.1);
  }
`;

const Button = styled.button<ButtonProps>`
  width: 50 px;
  height: auto;
  border-radius: 7px;
  margin-left: 10px;
  cursor: pointer;
  background: ${({ isDeleteType }) =>
    isDeleteType ? "rgb(255, 0, 0, 0.3)" : "gb(255, 255, 255, 0.1) "};
`;
const CommentsCard = styled.div`
  padding-left: 10px;
  padding-bottom: 20px;
  font-size: 14px;
  text-align: center;
  margin-right: 30px;
`;
const Buttons = styled.div`
  display: flex;
  justify-content: center;
`;
