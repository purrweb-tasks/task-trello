import React from "react";

import { Field, Form } from "react-final-form";
import styled, { css } from "styled-components";

import {
  selectors,
  actions,
  useAppSelector,
  useAppDispatch,
} from "../../store";
import Comment from "../Comment";
import { InputField } from "../UI";

const validateComment = (values: { newComment: string }) => {
  const error = { newComment: "" };
  if (!values.newComment) {
    return error;
  }
};
const validateCardName = (values: { cardName: string }) => {
  const error = { cardName: "Required field" };
  if (!values.cardName) {
    return error;
  }
};

const FullCard: React.FC<FullCardProps> = ({
  idCard,
  name,
  author,
  description,
}) => {
  const comments = useAppSelector(selectors.comment.selectByCardId(idCard));
  const dispatch = useAppDispatch();
  const commentAuthor = useAppSelector(selectors.user.getUser);

  const handleAddComment = (values: { newComment: string }) => {
    dispatch(
      actions.comment.addComment({
        idCard,
        contentComment: values.newComment,
        author: commentAuthor,
      })
    );
    values.newComment = "";
  };

  const handleCleanDescriptionCard = () => {
    dispatch(
      actions.card.updateDescriptionCard({ idCard, newDescription: "" })
    );
  };
  const handleChangeNameCard = (values: { cardName: string }) => {
    dispatch(actions.card.updateNameCard({ idCard, newName: values.cardName }));
  };
  const handleChangeDescriptionCard = (values: { descriptionCard: string }) => {
    dispatch(
      actions.card.updateDescriptionCard({
        idCard,
        newDescription: values.descriptionCard,
      })
    );
  };

  const renderComments = () => {
    if (!comments) {
      return null;
    }
    return comments.map((comment) => (
      <Comment
        idCard={idCard}
        id={comment.id}
        author={comment.author}
        content={comment.content}
        key={comment.id}
      />
    ));
  };

  return (
    <Root>
      <Form
        validate={validateCardName}
        onSubmit={handleChangeNameCard}
        initialValues={{ cardName: name }}
        render={({ handleSubmit, form }) => (
          <Field
            name="cardName"
            component={InputField}
            onBlur={() => {
              handleSubmit();
              form.reset();
            }}
          />
        )}
      />
      <Author>Author: {author}</Author>
      <Description>
        <DescriptionHead>Description</DescriptionHead>
        <Form
          onSubmit={handleChangeDescriptionCard}
          initialValues={{ descriptionCard: description }}
          render={({ handleSubmit }) => (
            <Field
              CSS={DescriptionStyles}
              name="descriptionCard"
              component={InputField}
              onBlur={handleSubmit}
              isTextarea
              placeholder="Description"
            />
          )}
        />
        <CleanDescription onClick={handleCleanDescriptionCard}>
          clear
        </CleanDescription>
      </Description>
      <Form
        onSubmit={handleAddComment}
        validate={validateComment}
        initialValues={{ newComment: "" }}
        render={({ handleSubmit, pristine }) => (
          <NewCommentForm onSubmit={handleSubmit}>
            <Field
              name="newComment"
              component={InputField}
              CSS={CommentStyles}
              placeholder="New comment"
              isTextarea
            />
            <CommentButton disabled={pristine}>comment</CommentButton>
          </NewCommentForm>
        )}
      />
      {renderComments()}
    </Root>
  );
};

export default FullCard;

type FullCardProps = {
  idCard: string;
  name: string;
  author: string;
  description: string;
};

const Root = styled.div`
  background-color: rgb(0, 0, 0, 0.1);
  padding: 10px;
  border-radius: 10px;
  width: 90%;
  max-height: 60vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  overflow: hidden;
  overflow-y: scroll;
  padding: 20px;
`;

const DescriptionHead = styled.div`
  font-size: 20;
  padding-left: 2%;
`;

const Author = styled.div`
  font-size: 20px;
  padding-top: 20px;
  padding-bottom: 20px;
`;

const Description = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
`;
const DescriptionStyles = css`
  background-color: white;
  padding: 5px;
  height: 200px;
  width: 300px;
  border-radius: 8px;
  margin: 5px;
  border: none;
  white-space: wrap;
  resize: none;
`;
const CommentStyles = css`
  background-color: white;
  padding: 5px;
  height: 40px;
  width: 300px;
  font-size: 12px;
  border-radius: 8px;
  margin: 5px;
  border: none;
  white-space: wrap;
  resize: none;
`;
const CleanDescription = styled.button`
  background-color: rgb(0, 0, 0, 0.1);
  border-radius: 5px;
  border: 2px black solid;
  cursor: pointer;
  position: absolute;
  right: -20%;
  bottom: 0;
`;

const NewCommentForm = styled.form`
  margin-bottom: 20px;
  padding-top: 50px;
  display: flex;
  margin-left: 80px;
`;

const CommentButton = styled.button`
  background-color: rgb(0, 0, 0, 0.1);
  margin: 10px;
  border: none;
  border-radius: 5px;
  cursor: pointer;
  height: 25px;
`;
