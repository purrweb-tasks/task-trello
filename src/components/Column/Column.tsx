import React from "react";

import { Form, Field } from "react-final-form";
import styled, { css } from "styled-components";

import { Card, InputField } from "..";
import {
  selectors,
  actions,
  useAppSelector,
  useAppDispatch,
} from "../../store";

const validateColumnName = (values: { columnName: string }) => {
  const error = { columnName: "Required field" };
  if (!values.columnName) {
    return error;
  }
};
const validateNameCard = (values: { nameNewCard: string }) => {
  const error = { nameNewCard: "" };
  if (!values.nameNewCard) {
    return error;
  }
};

const Column: React.FC<ColumnProps> = ({ id, name }) => {
  const cards = useAppSelector(selectors.card.selectByColumnId(id));
  const author = useAppSelector(selectors.user.getUser);
  const dispatch = useAppDispatch();

  const handleAddCard = (values: { nameNewCard: string }) => {
    dispatch(
      actions.card.addCard({
        columnId: id,
        nameCard: values.nameNewCard,
        author,
      })
    );
    values.nameNewCard = "";
  };
  const handleUpdateName = (values: { columnName: string }) => {
    dispatch(
      actions.column.updateColumn({ newName: values.columnName, columnId: id })
    );
  };

  const renderCards = () => {
    if (!cards) {
      return null;
    }
    return cards.map((card) => (
      <Card
        id={card.id}
        name={card.name}
        author={card.author}
        description={card.description}
        comments={card.comments}
        key={card.id}
      />
    ));
  };

  return (
    <Root>
      <Form
        onSubmit={handleUpdateName}
        initialValues={{ columnName: name }}
        validate={validateColumnName}
        render={({ handleSubmit, form }) => (
          <Field
            name="columnName"
            component={InputField}
            onBlur={() => {
              handleSubmit();
              form.reset();
            }}
          />
        )}
      />
      <Form
        onSubmit={handleAddCard}
        validate={validateNameCard}
        render={({ handleSubmit, pristine }) => (
          <CreateCard onSubmit={handleSubmit}>
            <Field
              name="nameNewCard"
              component={InputField}
              CSS={CardNameStyles}
              placeholder="new card"
            />
            <CreateButton disabled={pristine}>Create</CreateButton>
          </CreateCard>
        )}
      />
      {renderCards()}
    </Root>
  );
};

export default Column;

type ColumnProps = {
  id: string;
  name: string;
};

const Root = styled.div`
  background: rgb(0, 0, 0, 0.1);
  padding: 10px;
  display: flex;
  flex-direction: column;
  align-items: center;
  border-radius: 7px;
  margin: 10px;
  height: max-content;
`;
const CreateCard = styled.form`
  display: flex;
  padding-bottom: 10px;
  padding-top: 10px;
`;
const CardNameStyles = css`
  display: flex;
  justify-content: center;
  height: 10px;
  font-family: Arial, Helvetica, sans-serif;
  font-weight: 300;
  font-size: 15px;
  width: 70%;
  border: none;
  background-color: white;
  border: 1px rgb(255, 255, 255, 0.4) solid;
  margin-top: 10px;
`;
const CreateButton = styled.button`
  background-color: rgb(255, 255, 255, 0.4);
  border-radius: 10px;
  border: 2px rgb(0, 0, 0, 0.2) solid;
  margin-top: 10px;
  height: 20px;
  cursor: pointer;
  :hover {
    border: 2px gray solid;
  }
`;
